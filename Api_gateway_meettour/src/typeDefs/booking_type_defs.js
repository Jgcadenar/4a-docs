const {gql} = require('apollo-server');
const bookingTypeDefs = gql `
    type ResponseBooking{
        id: String!
        tourName: String!
        dateTour: String!
        dateReserva: String!
        username: String!
        value: Int!
        price: Int!
    }

    type DeleteResponse{
        message : String
    }

    type PutResponse{
        message : String
    }

    input CreateBookingInput{
        id: String!
        tourName: String!
        username: String!
        value: Int!
    } 

    input PutBookingInput{
        value: Int!
    }

    input DeleteBookingInput{
        username: String!
        id: String!
    }

    extend type Mutation{
        newBooking(createInput: CreateBookingInput!): ResponseBooking!
        putBooking(modInput: PutBookingInput!, username : String!, id : String! ) : PutResponse
        deleteBooking(username : String!, id : String!) : DeleteResponse 
    }

    extend type Query{
        bookingByusername(username : String!) : [ResponseBooking]
    }

`;

module.exports = bookingTypeDefs;