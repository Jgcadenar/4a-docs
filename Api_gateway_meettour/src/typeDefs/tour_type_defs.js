const {gql} = require('apollo-server');
const tourTypeDefs = gql `
    type ResponseTour{
        tourName: String! 
        dateTour: String!
        quota: Int!
        price: Int!
    }

    input CreateTourInput{
        tourName: String! 
        dateTour: String!
        quota: Int!
        price: Int!
    } 

    extend type Mutation{
        newTour(tourInput: CreateTourInput!): ResponseTour!
    }

    extend type Query{
        tourByTourname(tourName : String!) : ResponseTour!
    }

`;

module.exports = tourTypeDefs;