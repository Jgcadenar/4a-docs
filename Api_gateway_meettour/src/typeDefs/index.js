const bookingTypeDefs = require('./booking_type_defs');
const authTypeDefs = require('./auth_type_defs');
const tourTypeDefs = require('./tour_type_defs');

const schemasArrays = [bookingTypeDefs, authTypeDefs, tourTypeDefs];

module.exports = schemasArrays;