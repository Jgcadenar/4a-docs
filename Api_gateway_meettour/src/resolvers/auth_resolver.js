const usersResolvers = {

    Query:{
        userDetailById: async(_,{id},{dataSources}) => {
            return dataSources.AuthAPI.getUser(id);
        },
        
    },
    
    Mutation:{
        signUpUser: async(_,{userInput},{dataSources}) =>{
           const authInput = {
                username: userInput.username,
                password: userInput.password,
                name: userInput.name,
                email: userInput.email
            }
            return await dataSources.AuthAPI.createUser(authInput);
        },
        logIn: (_,{credentials},{dataSources}) =>{
            return dataSources.AuthAPI.authRequest(credentials);
        },
        refreshToken: (_,{refresh},{dataSources}) =>{
            return dataSources.AuthAPI.refreshToken(refresh);
        }
    }
};

module.exports = usersResolvers;