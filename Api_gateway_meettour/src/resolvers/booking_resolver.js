const bookingResolvers = {
    Query:{
        bookingByusername : async(_, {username}, {dataSources}) => {
            return dataSources.BookingAPI.getBooking(username);
        },
    },


    Mutation: {

        newBooking: async(_,{createInput},{dataSources}) => {
            const bookingInput = {
                id: createInput.id,
                tourName: createInput.tourName,
                username: createInput.username,
                value: createInput.value
            }
            return dataSources.BookingAPI.postBooking(bookingInput);
        },

        putBooking: async(_, {modInput, username, id}, {dataSources}) => {
            const newData = {
                value : modInput.value
            }
            return await dataSources.BookingAPI.putBooking(newData, username, id);
        },
        
        deleteBooking: async(_, {username, id}, {dataSources}) => {
            return dataSources.BookingAPI.deleteBooking(username, id);
        }
        
    }

};

module.exports = bookingResolvers;