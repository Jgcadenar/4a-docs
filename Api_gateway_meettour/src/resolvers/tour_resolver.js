const tourResolvers = {

    Query:{
        tourByTourname : async(_, {tourName}, {dataSources}) => {
            return dataSources.BookingAPI.getTour(tourName);
        },
    },


    Mutation: {

        newTour: async(_,{tourInput},{dataSources}) => {
            const toursInput = {
                tourName: tourInput.tourName,  
                dateTour: tourInput.dateTour,
                quota: tourInput.quota,
                price: tourInput.price
            }
            return dataSources.BookingAPI.postTour(toursInput);
        },
    }

};

module.exports = tourResolvers;