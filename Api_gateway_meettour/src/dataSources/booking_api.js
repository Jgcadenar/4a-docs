const {RESTDataSource} = require('apollo-datasource-rest');
const serverConf = require('../server');

class BookingAPI extends RESTDataSource {
    constructor(){
        super();
        this.baseURL = serverConf.booking_api_url;
    }

    async postBooking (booking){
        booking = new Object(JSON.parse(JSON.stringify(booking)));
        return await this.post(`bookings`, booking);
    }

    async getBooking(username){
        return await this.get(`/bookings/${username}`, username);
    }

    async putBooking(newBooking, username, id){
        newBooking = new Object(JSON.parse(JSON.stringify(newBooking)));
        return await this.put(`/bookings/${username}/${id}`, newBooking, username, id);
    }
    
    async deleteBooking(username, id){
        return await this.delete(`/bookings/${username}/${id}`, username, id);
    }

    async postTour (tour){
        tour = new Object(JSON.parse(JSON.stringify(tour)));
        return await this.post(`tours`, tour);
    } 

    async getTour(tourname){
        return await this.get(`/tours/${tourname}`, tourname);
    }


}

module.exports = BookingAPI;