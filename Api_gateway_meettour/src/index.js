const {ApolloServer} = require('apollo-server');

const typeDefs = require('./typeDefs')
const resolvers = require('./resolvers')
const AuthAPI = require('./dataSources/auth_api')
const BookingAPI = require('./dataSources/booking_api')
const Authentication = require('./utils/authentication')

const server = new ApolloServer({
    context: Authentication,
    typeDefs,
    resolvers,
    dataSources: () =>({
        BookingAPI: new BookingAPI(),
        AuthAPI: new AuthAPI(),
    }),
    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then (({url}) => {
    console.log(`🚀Server ready at ${url}`);
}
);