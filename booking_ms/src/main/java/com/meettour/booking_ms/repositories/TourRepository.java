package com.meettour.booking_ms.repositories;

import com.meettour.booking_ms.models.Tour;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TourRepository extends MongoRepository<Tour, String>{ }
