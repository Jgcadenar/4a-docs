package com.meettour.booking_ms.repositories;

import com.meettour.booking_ms.models.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BookingRepository extends MongoRepository<Booking, String> {
    List<Booking> findByusername(String username);
}
