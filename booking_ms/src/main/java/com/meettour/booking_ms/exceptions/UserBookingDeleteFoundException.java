package com.meettour.booking_ms.exceptions;

public class UserBookingDeleteFoundException extends RuntimeException {

    public UserBookingDeleteFoundException(String message) {
        super(message);
    }

}
