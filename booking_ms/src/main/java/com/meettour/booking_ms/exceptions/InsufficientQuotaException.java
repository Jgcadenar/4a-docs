package com.meettour.booking_ms.exceptions;

public class InsufficientQuotaException extends RuntimeException{
    public InsufficientQuotaException(String message) {
        super(message);
    }
}
