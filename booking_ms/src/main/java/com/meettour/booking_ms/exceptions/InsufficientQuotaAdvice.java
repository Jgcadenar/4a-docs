package com.meettour.booking_ms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseBody
public class InsufficientQuotaAdvice {
    @ResponseBody
    @ExceptionHandler(InsufficientQuotaException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String InsufficientQuotaAdvice(InsufficientQuotaException ex){
        return ex.getMessage();
    }
}
