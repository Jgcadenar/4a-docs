package com.meettour.booking_ms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseBody

public class UserBookingDeleteFoundAdvice {

    @ResponseBody
    @ExceptionHandler(UserBookingDeleteFoundException.class)
    @ResponseStatus(HttpStatus.ACCEPTED)
    String EntityNotFoundAdvice(UserBookingDeleteFoundException ex){return ex.getMessage();
    }

}
