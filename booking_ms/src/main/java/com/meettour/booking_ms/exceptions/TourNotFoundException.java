package com.meettour.booking_ms.exceptions;

public class TourNotFoundException extends RuntimeException{

    public TourNotFoundException(String message) {
        super(message);
    }

}
