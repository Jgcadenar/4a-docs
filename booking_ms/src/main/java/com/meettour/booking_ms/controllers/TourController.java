package com.meettour.booking_ms.controllers;

import com.meettour.booking_ms.exceptions.TourNotFoundException;
import com.meettour.booking_ms.models.Tour;
import com.meettour.booking_ms.repositories.TourRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class TourController {

    private final TourRepository tourRepository;

    public TourController(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    @PostMapping("/tours")
    Tour newTour (@RequestBody Tour tour){
        return tourRepository.save(tour);
    }

    @GetMapping("/tours/{tourName}")
    Tour getTours (@PathVariable String tourName){
        return tourRepository.findById(tourName).orElseThrow(() -> new TourNotFoundException("No se encontro un tour con el nombre: " + tourName));
    }

}
