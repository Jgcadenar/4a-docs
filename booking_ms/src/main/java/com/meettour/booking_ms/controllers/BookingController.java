package com.meettour.booking_ms.controllers;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.meettour.booking_ms.exceptions.BookingNotFoundException;
import com.meettour.booking_ms.exceptions.InsufficientQuotaException;
import com.meettour.booking_ms.exceptions.TourNotFoundException;
import com.meettour.booking_ms.exceptions.UserBookingDeleteFoundException;
import com.meettour.booking_ms.models.Booking;
import com.meettour.booking_ms.models.Tour;
import com.meettour.booking_ms.repositories.BookingRepository;
import com.meettour.booking_ms.repositories.TourRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class BookingController {

    private final BookingRepository bookingRepository;
    private final TourRepository tourRepository;

    public BookingController(BookingRepository bookingRepository, TourRepository tourRepository) {
        this.bookingRepository = bookingRepository;
        this.tourRepository = tourRepository;
    }

    @PostMapping("/bookings")
    Booking newBooking (@RequestBody Booking booking){

        Tour tourName = tourRepository.findById(booking.getTourName()).orElse(null);

        if (tourName == null) {
            throw new TourNotFoundException("No se encontró un tour con ese nombre: " + booking.getTourName());
        }

        Tour tourQuota = tourRepository.findById(booking.getTourName()).orElse(null);

        if (tourQuota.getQuota() < booking.getValue()) {
            throw new InsufficientQuotaException("No se puede realiza la reserva por falta de cupos en el tour: " + booking.getTourName());
        }

        tourQuota.setQuota(tourQuota.getQuota() - booking.getValue());
        tourRepository.save(tourQuota);
        booking.setDateTour(tourQuota.getDateTour());
        booking.setPrice(tourQuota.getPrice() * booking.getValue());
        booking.setDateReserva(new Date());
        return bookingRepository.save(booking);
    }

    @GetMapping("/bookings/{username}")
    List<Booking> getTours (@PathVariable String username) {
        return bookingRepository.findByusername(username);
    }

    /*@GetMapping("/bookings/{id}")
    Booking getToursByid (@PathVariable String id) {
        return bookingRepository.findById(id).orElse(null);
    }*/

    @DeleteMapping("/bookings/{username}/{id}")
    public void remove(@PathVariable Map<String ,String> pathVarsMap) {
        String id = pathVarsMap.get("id");
        String username = pathVarsMap.get("username");

        List<Booking> userBooking = bookingRepository.findByusername(username);

        for (Booking i: userBooking) {
            String valor = i.getId();
            Integer newQuota = i.getValue();

            if (valor.equals(id)) {
                bookingRepository.deleteById(id);
                Tour tourName = tourRepository.findById(i.getTourName()).orElse(null);
                tourName.setQuota(tourName.getQuota() + newQuota);
                tourRepository.save(tourName);
                throw new UserBookingDeleteFoundException("El registro de la reserva con id: "+ id + " se eliminó satisfactoriamente");
            }
        }
    }

    @PutMapping("/bookings/{username}/{id}")
    public void actualBooking (@RequestBody Booking booking, @PathVariable Map<String ,String> pathVarsMap) {
        String id = pathVarsMap.get("id");
        String userName = pathVarsMap.get("username");
        List<Booking> userBooking = bookingRepository.findByusername(userName);
        for (Booking i: userBooking) {
            String valor = i.getId();
            Integer newQuota = i.getValue();
            if (valor.equals(id)) {

                Integer valueBooking = booking.getValue();
                Integer diferencia = newQuota - valueBooking;
                Tour tourName = tourRepository.findById(i.getTourName()).orElse(null);
                Integer newPrice = tourName.getPrice();

                if (diferencia > 0){
                    if (tourName.getQuota() < diferencia) {
                        throw new InsufficientQuotaException("No se puede realiza la reserva por falta de cupos en el tour: " + i.getTourName());
                    }
                    tourName.setQuota(tourName.getQuota() - diferencia);
                    tourRepository.save(tourName);

                    i.setPrice(newPrice * newQuota);
                    i.setValue(newQuota);
                    bookingRepository.save(i);
                }
                else if (diferencia < 0){
                    tourName.setQuota(tourName.getQuota() + diferencia);
                    tourRepository.save(tourName);

                    i.setPrice(newPrice * valueBooking);
                    i.setValue(valueBooking);
                    bookingRepository.save(i);
                }

            }
        }
    }

}
