package com.meettour.booking_ms.models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Tour {

    /*Attributes*/
    @Id
    private String tourName;
    private Date dateTour;
    private Integer quota;
    private Integer price;


    /*Constructor*/
    public Tour(String tourName, Date dateTour, Integer quota, Integer price, String paymentMethod) {
        this.tourName = tourName;
        this.dateTour = dateTour;
        this.quota = quota;
        this.price = price;

    }
    public Tour() {
        this.tourName = tourName;
        this.dateTour = dateTour;
        this.quota = quota;
        this.price = price;
    }

    /*Getter and Setter*/

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public Date getDateTour() {
        return dateTour;
    }

    public void setDateTour(Date dateTour) {
        this.dateTour = dateTour;
    }

    public Integer getQuota() {
        return quota;
    }

    public void setQuota(Integer quota) {
        this.quota = quota;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
