package com.meettour.booking_ms.models;

import org.springframework.data.annotation.Id;
import java.util.Date;

public class Booking {

    /*Attributes*/
    @Id
    private String id;
    private String tourName;
    private Date dateTour;
    private Date dateReserva;
    private String username;
    private Integer value;
    private Integer price;

    /*Constructor*/

    public Booking(String id, String tourName, Date dateTour, Date dateReserva, String username, Integer value, Integer price) {
        this.id = id;
        this.tourName = tourName;
        this.dateTour = dateTour;
        this.dateReserva = dateReserva;
        this.username = username;
        this.value = value;
        this.price = price;
    }
    /*Getter and Setter*/

    public String getId() {return id; }

    public void setId(String id) {this.id = id; }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public Date getDateTour() {
        return dateTour;
    }

    public void setDateTour(Date dateTour) {
        this.dateTour = dateTour;
    }

    public Date getDateReserva() {
        return dateReserva;
    }

    public void setDateReserva(Date dateReserva) {
        this.dateReserva = dateReserva;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {this.username = username;}

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}//Cierra class Booking
